<?php
/*
Plugin Name: مدیریت تبلیفات وردپرس
Plugin URI: armiinzahedi.dev
Description:پلاگین مدیریت تبلیغات در وردپرس
Author: Armin Zahedi
Version: 1.0.0
Author URI:  http://armiinzahedi.dev
*/

defined('ABSPATH') || exit('NO ACCESS');

//define constants
define('WPADS_DIR',trailingslashit(plugin_dir_path(__FILE__)));
define('WPADS_INC',trailingslashit(WPADS_DIR.'includes'));
define('WPADS_LIBS',trailingslashit(WPADS_DIR.'libs'));
define('WPADS_TPLS',trailingslashit(WPADS_DIR.'tpls'));
$upload_path=wp_upload_dir();
define('WPADS_URL',trailingslashit(plugin_dir_url(__FILE__)));
define('WPADS_BANNER_URL',trailingslashit($upload_path['baseurl'].DIRECTORY_SEPARATOR.'wpads'));
define('WPADS_ASSETS',trailingslashit(WPADS_URL.'assets'));

//define main hooks
add_action('init',function (){
    ob_start();
},1);
function wpads_activation(){
    $options = get_option('wpads_options');
    if(empty($options) || ( is_array($options) && count($options) == 0)){
        $options = array();
        update_option('wpads_options',$options);
    }
}
function wpads_deactivation(){

}
register_activation_hook(__FILE__,'wpads_activation');
register_deactivation_hook(__FILE__,'wpads_deactivation');

//do includes
if(is_admin()){
    if(!defined('DOING_AJAX')){
        include WPADS_INC.'admin_page.php';
        include WPADS_INC.'admin_menu.php';
        include WPADS_INC.'functions.php';
    }
}
include WPADS_INC.'user_functions.php';
include WPADS_INC.'shortcodes.php';