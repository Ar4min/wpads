<?php
function wpads_show_callback($atts,$content=null){
    $args = shortcode_atts(array(
        'id'  => 0,
    ),$atts);
    if(!intval($args['id'])){
        return false;
    }
    $zone_id = intval($args['id']);
    global $wpdb,$table_prefix;
    $zone_type = $wpdb->get_var($wpdb->prepare("SELECT zone_type FROM {$table_prefix}wpads_zone WHERE zone_id=%d",$zone_id));
    $zone_banners = $wpdb->get_results($wpdb->prepare("SELECT 
                                        a.*,
                                        z.zone_width,
                                        z.zone_height 
                                        FROM {$table_prefix}wpads_advertise a
                                        JOIN {$table_prefix}wpads_zone z
                                        ON a.zone_id = z.zone_id
                                        WHERE a.zone_id=%d",$zone_id));
    if(!$zone_banners || count($zone_banners) == 0){
        return false;
    }
    return wpads_show_zone_ads($zone_type,$zone_banners);

}
add_shortcode('wpads_show','wpads_show_callback');